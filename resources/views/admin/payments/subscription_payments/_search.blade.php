<div class="row pt-3">

    <div class="col-6"></div>

    <div class="col-6">

        <form class="search-button" action="{{route('admin.subscription_payments.index')}}" method="GET" role="search">
            <!-- {{csrf_field()}} -->


            <div class="input-group">


                <div class="col-4" style="display:none;">

                    <select class="form-control select2" name="payment_mode">

                        <option class="select-color text-capitalize" value="">{{tr('select_payment_mode')}}</option>

                        <option class="select-color text-capitalize" value="{{COD}}" @if(Request::get('payment_mode') ==  COD) selected  @endif>{{tr('cod')}}</option>

                        <option class="select-color text-capitalize" value="{{RAZORPAY}}" @if(Request::get('payment_mode') ==  RAZORPAY)  selected @endif>{{tr('razorpay')}}</option>

                        <option class="select-color text-capitalize" value="{{CARD}}" @if(Request::get('payment_mode') ==  CARD) selected  @endif>{{tr('card')}}</option>


                    </select>

                </div>


                <input type="text" class="form-control" name="search_key" value="{{Request::get('search_key')}}" placeholder="{{tr('subscription_payments_search_placeholder')}}">

                <span class="input-group-btn">
                    &nbsp

                    <button type="submit" class="btn btn-primary">
                        {{tr('search')}}
                    </button>

                    <a class="btn btn-primary" href="{{route('admin.subscription_payments.index')}}">{{tr('clear')}}
                    </a>

                </span>
            </div>

        </form>

    </div>


</div>

<div class="col-6">
    @if(Request::has('search_key'))
    <p class="text-muted">Search results for <b>{{Request::get('search_key')}}</b></p>
    @endif
</div>